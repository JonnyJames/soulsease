<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * Template Products
 * 
 * Access original fields: $mod_settings
 */

$fields_default = array(
	'mod_title_products' => '',
	'query_products' => 'all',
	'template_products' => 'list',
	'hide_free_products' => 'no',
	'layout_products' => 'list-post',
	'category_products' => '',
	'post_per_page_products' => 6,
	'offset_products' => 0,
	'order_products' => 'ASC',
	'orderby_products' => 'title',
	'description_products' => 'none',
	'hide_feat_img_products' => 'no',
	'image_size_products' => '',
	'img_width_products' => '',
	'img_height_products' => '',
	'unlink_feat_img_products' => 'no',
	'hide_post_title_products' => 'no',
	'unlink_post_title_products' => 'no',
	'hide_price_products' => 'no',
	'hide_add_to_cart_products' => 'no',
	'hide_rating_products' => 'no',
	'hide_sales_badge' => 'no',
	// slider settings
	'layout_slider' => '',
	'visible_opt_slider' => '',
	'auto_scroll_opt_slider' => 0,
	'scroll_opt_slider' => '',
	'speed_opt_slider' => '',
	'effect_slider' => 'scroll',
	'pause_on_hover_slider' => 'resume',
	'wrap_slider' => 'yes',
	'show_nav_slider' => 'yes',
	'show_arrow_slider' => 'yes',
	'left_margin_slider' => '',
	'right_margin_slider' => '',
	'animation_effect' => '',
	'css_products' => '',
);

if ( isset( $mod_settings['category_products'] ) )	
	$mod_settings['category_products'] = $this->get_param_value( $mod_settings['category_products'] );

$fields_args = wp_parse_args( $mod_settings, $fields_default );
extract( $fields_args, EXTR_SKIP );

$temp_terms = explode( ',', $category_products );
$terms = array();
$is_string = false;
foreach ( $temp_terms as $t ) {
	if ( ! is_numeric( $t ) )
		$is_string = true;
	if ( '' != $t ) {
		array_push( $terms, trim( $t ) );
	}
}
$tax_field = ( $is_string ) ? 'slug' : 'id';

$query_args = array(
	'post_type' => 'product',
	'posts_per_page' => $post_per_page_products,
	'order' => $order_products,
);
$query_args['meta_query'][] = WC()->query->stock_status_meta_query();
$query_args['meta_query']   = array_filter( $query_args['meta_query'] );

if ( count( $terms ) > 0 && ! in_array( '0', $terms ) ) {
	$query_args['tax_query'] = array(
		array(
			'taxonomy' => 'product_cat',
			'field' => $tax_field,
			'terms' => $terms
		)
	);
}

if( $query_products == 'onsale' ) {
	$product_ids_on_sale = wc_get_product_ids_on_sale();
	$product_ids_on_sale[] = 0;
	$query_args['post__in'] = $product_ids_on_sale;
} elseif( $query_products == 'featured' ) {
	$query_args['meta_query'][] = array(
		'key'   => '_featured',
		'value' => 'yes'
	);
}

switch ( $orderby_products ) {
	case 'price' :
		$query_args['meta_key'] = '_price';
		$query_args['orderby']  = 'meta_value_num';
		break;
	case 'sales' :
		$query_args['meta_key'] = 'total_sales';
		$query_args['orderby']  = 'meta_value_num';
		break;
	default :
		$query_args['orderby']  = $orderby_products;
}

if ( $hide_free_products == 'yes' ) {
	$query_args['meta_query'][] = array(
		'key'     => '_price',
		'value'   => 0,
		'compare' => '>',
		'type'    => 'DECIMAL',
	);
}

	// render the template
	$this->retrieve_template( 'template-'.$mod_name.'-'.$template_products.'.php', array(
		'module_ID' => $module_ID,
		'mod_name' => $mod_name,
		'query_args' => $query_args,
		'settings' => ( isset( $fields_args ) ? $fields_args : array() )
	), '', '', true );